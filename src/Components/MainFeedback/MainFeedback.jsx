import React from "react";
import "./MainFeedback.css"

const MainFeedback = () => {
    return (
        <div className="mainFeedBackBackground">
            <div className="mainFeedBackBorder">
                <h1>
                    Оставьте отзыв!
                </h1>

                <p className="settingTextParagraph">
                    Вы можете оставить отзыв о нашей работе в группе в ВК
                </p>

                <div className="mainFeedBackGrid">
                    <a href="https://vk.com/llckremsoft">
                        <div className="mainFeedBackFlex">

                        <img
                            src="./image/icons/vk.png"
                            alt=""
                            style={{gridArea: "imageVk"}}/>
                            <p style={{gridArea: "text1", width: "370px"}}>
                                Заходите в группу
                            </p>
                        </div>
                    </a>
                    <a href="https://vk.com/topic-212902495_48821988">
                        <div className="mainFeedBackFlex">
                        <img
                        style={{gridArea: "imageMessage"}}
                        src="./image/icons/chat.png"
                        alt=""/>

                        <p style={{gridArea: "text2"}}>
                            Оставляйте отзыв и делитесь впечятлениями
                        </p>
                    </div>
                    </a>

                </div>
                <p className="settingTextParagraphTwo">
                    Ваш отзыв поможет нам стать лучше!
                </p>
            </div>
        </div>
    )
}
export default MainFeedback