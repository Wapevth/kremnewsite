import React from "react";
import './WorkStagePage.css'
import CustomButton from "../CustomComponents/CustomButton/CustomButton";
import CustomLine from "../CustomComponents/CustomLine/CustomLine";

const WorkStagePage = () => {
    return (
        <div className="workStagePageBackground">
            <h1 className="workStagePageHeader">
                Этапы работ
            </h1>
            <div className="StageFlex">
                <CustomButton name="Отправить заявку" style={{width: "250px", height: "60px", fontSize: "24px"}}/>
                <CustomLine className="settingCustomLineApplication"/>
                <div className="StageGrid">
                    <div className="stageFlexItem">
                        <img style={{gridArea: "image"}} src="./image/icons/vibration.png" alt=""/>
                    </div>
                    <p style={{gridArea: "text"}}>Наш сотрудник вам позвонит </p>
                </div>
                <CustomLine className="settingCustomLineApplication"/>
                <div className="StageGrid">
                    <div className="stageFlexItem">
                        <img style={{gridArea: "image"}} src="./image/icons/approve-file.png" alt=""/>
                    </div>
                    <p style={{gridArea: "text"}}>Составляем ТЗ</p>
                </div>
                <CustomLine className="settingCustomLineApplication"/>
                <div className="StageGrid">
                    <div className="stageFlexItem">
                        <img style={{gridArea: "image"}} src="./image/icons/light-bulb.png" alt=""/>
                    </div>
                    <p style={{gridArea: "text"}}>Работа полным ходом</p>
                </div>
                <CustomLine className="settingCustomLineApplication"/>
                <div className="StageGrid">
                    <div className="stageFlexItem">
                        <img style={{gridArea: "image"}} src="./image/icons/purse.png" alt=""/>
                    </div>
                    <p style={{gridArea: "text"}}>Вы принимаете работу а мы получаем оплату</p>
                </div>
            </div>
        </div>
    )
}

export default WorkStagePage