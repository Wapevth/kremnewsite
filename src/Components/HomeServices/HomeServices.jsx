import './HomeServices.css'

const HomeServices = () => {
    return (
        <div className="homeServicesBackground">
            <h1 className="homeServicesHeader">
                Наши услуги
            </h1>
            <p className="homeServicesSubtitle">
                Разработка сайтов, приложений и ботов в Telegram под ваши нужды
            </p>
            <div className="homeServicesFlex">
                <div className="homeServicesGrid">
                    <div className="homeServicesGridTwo">
                        <img style={{gridArea: "image"}} src="./image/HomeServices/avatar1.jpg" alt="1"
                             className="settingImageGrid"/>
                        <p style={{gridArea: "text"}}>Лендинг или сайт визитка</p>
                        <img className="settingIconsGrid" style={{gridArea: "icons"}}
                             src="https://www.continucontent.nl/wp-content/uploads/2017/12/webteksten.png" alt=""/>
                    </div>
                    <div className="homeServicesGridTwo">
                        <img style={{gridArea: "image"}} src="./image/HomeServices/avatar2.jpg" alt="2"
                             className="settingImageGrid"/>
                        <p style={{gridArea: "text"}}>Многостраничный сайт</p>
                        <img className="settingIconsGrid" style={{gridArea: "icons"}}
                             src="https://www.continucontent.nl/wp-content/uploads/2017/12/webteksten.png" alt=""/>

                    </div>
                    <div className="homeServicesGridTwo">
                        <img style={{gridArea: "image"}} src="./image/HomeServices/avatar3.jpg" alt="3"
                             className="settingImageGrid"/>
                        <p style={{gridArea: "text"}}>Сайт-сервис</p>
                        <img className="settingIconsGrid" style={{gridArea: "icons"}}
                             src="https://www.continucontent.nl/wp-content/uploads/2017/12/webteksten.png" alt=""/>
                    </div>
                    <div className="homeServicesGridTwo">
                        <img style={{gridArea: "image"}} src="./image/HomeServices/avatar4.jpg" alt="4"
                             className="settingImageGrid"/>
                        <p style={{gridArea: "text"}}>Приложение сервис</p>
                        <img className="settingIconsGrid" style={{gridArea: "icons"}}
                             src="https://www.continucontent.nl/wp-content/uploads/2017/12/webteksten.png" alt=""/>
                    </div>
                    <div className="homeServicesGridTwo">
                        <img style={{gridArea: "image"}} src="./image/HomeServices/avatar5.jpg" alt="5"
                             className="settingImageGrid"/>
                        <p style={{gridArea: "text"}}>Приложение для самозанятых</p>
                        <img className="settingIconsGrid" style={{gridArea: "icons"}}
                             src="https://www.continucontent.nl/wp-content/uploads/2017/12/webteksten.png" alt=""/>
                    </div>
                    <div className="homeServicesGridTwo">
                        <img style={{gridArea: "image"}} src="./image/HomeServices/avatar6.jpg" alt="6"
                             className="settingImageGrid"/>
                        <p style={{gridArea: "text", whiteSpace:"nowrap"}}>Чат-бот в Telegram</p>
                        <img className="settingIconsGrid" style={{gridArea: "icons"}}
                             src="https://www.continucontent.nl/wp-content/uploads/2017/12/webteksten.png" alt=""/>
                    </div>
                </div>
            </div>
        </div>
    )
}
export default HomeServices