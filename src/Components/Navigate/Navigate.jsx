import React from "react";
import {Link, Outlet} from "react-router-dom";
import "./Navigate.css";
import CustomButton from "../CustomComponents/CustomButton/CustomButton";

const Navigate = () => {

    return (
        <nav>
            <div className="navigateBarSetting">
                <img src="./image/logo/logo.jpg" alt=""/>
                <div className="customLink">
                    <Link style={{color:"white",textDecoration:"none", marginRight:"4%"}} to="/">Услуги</Link>
                    <Link style={{color:"white",textDecoration:"none", marginRight:"4%"}} to="/">Портфолио</Link>
                    <Link style={{color:"white",textDecoration:"none", marginRight:"4%"}} to="/">О нас</Link>
                    <CustomButton name="Оставить заявку" className="settingButtonNavigate" style={{width:"180px", height:"40px", fontSize:"16px"}}/>
                </div>
            </div>

            <main>
                <Outlet/>
            </main>
        </nav>
    )
}
export default Navigate
