import {useState} from "react";
import Select from 'react-select';
import "./DropList.css"

const DropList = () => {
    const getValue = () => {
        return currentService ? options.find(s => s.value === currentService) : ""
    }
    const onChange = (newValue) => {
        setCurrentService(newValue.value)
    }
    const [currentService, setCurrentService] = useState('Выбор услуг')
    const options = [{
        value: 'LandingOrCardSite',
        label: 'Лендинг или сайт визитка'
    }, {
        value: 'MultiWebSite',
        label: 'Многостраничный сайт'
    }, {
        value: 'ServiceWebSite',
        label: 'Сайт-сервис'
    }, {
        value: 'ServiceApplication',
        label: 'Приложение сервис'
    }, {
        value: 'ApplicationForSelf-employed',
        label: 'Приложение для самозанятых '
    }, {
        value: 'TelegramChatBot',
        label: 'Чат-бот в Telegram'
    }, {
        value: 'GraduateWork',
        label: 'Дипломная работа'
    }, {
        value: 'CoursePaper',
        label: 'Курсовая работа'
    }]
    return (
        <div>
            <Select
                className="DropListSetting data-scrollbar"
                defaultValue={{value: 0, label: "Выберите услугу"}}
                onChange={onChange}
                isSearchable={false}
                value={getValue()}
                options={options}
                maxMenuHeight="140px"/>
        </div>
    )
}
export default DropList