import React from "react";
import './CustomLine.css'
const CustomLine = (props) => {
    return (
        <hr className={props.className}/>
    )
}
export default CustomLine