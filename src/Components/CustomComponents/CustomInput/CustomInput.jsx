import React from "react";
import "./CustomInput.css"

const CustomInput = (props) => {
    return (
        <input style={props.style} onChange={props.OnChange} className={props.className} value={props.value} type="text" placeholder={props.placeholder} />
    )
}
export default CustomInput