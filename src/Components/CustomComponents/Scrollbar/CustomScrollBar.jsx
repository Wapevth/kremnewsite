import {useEffect} from "react";
import Scrollbar from 'smooth-scrollbar';
import "./CustomScrollBar.css"

const CustomScrollBar = () => {
    const options = {
        damping: 0.07,
    }
    useEffect(()=>{
        Scrollbar.init(document.body, options);
    },[])

    return null;
}
export default CustomScrollBar