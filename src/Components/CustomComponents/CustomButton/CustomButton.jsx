import React from "react";
import "./CustomButton.css"

const CustomButton = (props) => {
    return (
        <button style={props.style} onClick={props.onClick} className={props.className}>
            {props.name}
        </button>
    )
}
export default CustomButton