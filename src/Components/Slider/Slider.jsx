import React from "react";
import { Swiper, SwiperSlide } from 'swiper/react';
import {Autoplay, Navigation, Pagination} from "swiper";
import './Slider.css'
import 'swiper/css/pagination';
import 'swiper/css/bundle'
import 'swiper/css/autoplay'



const Slider = () => {
  return (
      <Swiper
          modules={[Navigation, Pagination,Autoplay]}
          autoplay={{delay:4000,stopOnLastSlide:false, disableOnInteraction:false}}
          spaceBetween={10}
          speed={1200}
          loop={true}
          slidesPerView={1}
          pagination={{ clickable: true }}
      >
          <SwiperSlide>
              <img src="./image/slider/slidertest.png" alt="1"/>
          </SwiperSlide>
          <SwiperSlide>
              <img src="./image/slider/slidertest.png" alt="2"/>
          </SwiperSlide>
          <SwiperSlide>
              <img src="./image/slider/slidertest.png" alt="3"/>
          </SwiperSlide>
          <SwiperSlide>
              <img src="./image/slider/slidertest.png" alt="4"/>
          </SwiperSlide>
      </Swiper>
  )
}
export default Slider