import React, {useState} from "react";
import CustomButton from "../CustomComponents/CustomButton/CustomButton";
import CustomInput from "../CustomComponents/CustomInput/CustomInput";
import "./MainApplication.css"
import DropList from "../CustomComponents/DropList/DropList";

const MainApplication = () => {
    return (
        <div className="mainApplicationBackground">
            <div className="mainApplicationText">
            <h1>Разработаем продукт по вашим запросам</h1>
            <p style={{fontSize: "36px", fontWeight: "700", color: "#434343"}}>Заполните форму,
                наш сотрудник позвонит
                вам и проконсультирует по всем вопросам</p>
            </div>

            <div className="mainApplicationGrid">

                <div className="mainApplicationAboutFlex" style={{gridArea:"FIO"}}>
                    <CustomInput style={{marginTop: "3.5%"}} placeholder="ФИО*"/>
                    <CustomInput style={{marginTop: "3.5%"}}
                                 placeholder="Номер телефона для связи*"/>
                    <DropList style={{marginTop: "3.5%"}} type="text"/>
                    <CustomButton name="Отправить заявку"
                                  style={{
                                      width: "250px",
                                      height: "60px",
                                      fontSize: "24px",
                                      marginTop: "5.5%",
                                      marginBottom:"3%"
                                  }}/>
                </div>

                <div className="mainApplicationWithUsFlex">
                    <div className="mainApplicationWithUs">
                        <div className="mainApplicationWith" style={{gridArea: "WithUs"}}>
                            <p className="settingMainApplicationParagraph">Номер для связи с нами:</p>
                            <a href="tel:+79950635883">+7 (995) 063-58-83</a>
                            <a href="tel:+79959250701">+7 (995) 925-07-01</a>
                            <p className="settingMainApplicationText">Пн-Пт: с 6-00 до 18-00 МСК</p>
                            <p className="settingMainApplicationText">Сб: с 6-00 до 16-00 МСК</p>
                            <p className="settingMainApplicationText">Вс: выходной</p>
                        </div>
                    </div>
                </div>


            </div>

        </div>
    )
}
export default MainApplication