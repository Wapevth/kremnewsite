import React from "react";
import "./Footer.css"

const Footer = () => {
    return (
        <footer>
            <div>
                <p>Copyright © 2022 Kremnev Soft. All rights reserved.</p>
            </div>
            <div>
                <a href="">Политика кондефициальности</a>
            </div>
            <div>
                <a href=""> Обработка персональных данных</a>

            </div>

        </footer>
    )
}
export default Footer