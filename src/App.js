import React, {useEffect} from "react";
import {Routes, Route, useLocation, Outlet} from "react-router-dom";
import './App.css';
import Navigate from "./Components/Navigate/Navigate";
import Slider from "./Components/Slider/Slider";
import HomeServices from "./Components/HomeServices/HomeServices";
import WorkStagePage from "./Components/WorkStagePage/WorkStagePage";
import MainFeedback from "./Components/MainFeedback/MainFeedback";
import MainApplication from "./Components/MainApplication/MainApplication";
import Footer from "./Components/Footer/Footer";
import Modal from "./Components/CustomComponents/Modal/Modal";
import DropList from "./Components/CustomComponents/DropList/DropList";
import CustomScrollBar from "./Components/CustomComponents/Scrollbar/CustomScrollBar";

function App() {
  return (
    <div className="App">
        <Navigate/>
        <Slider/>
        <HomeServices/>
        <WorkStagePage/>
        <MainFeedback/>
        <MainApplication/>
        <Footer/>
    </div>
  );
}

export default App;
